import DateTimeFormat = Intl.DateTimeFormat;

export class DriverModel {
  name: string;
  occupation: string;
  birthDate: DateTimeFormat;
}
