import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SideMenuComponent } from './side-menu.component';
import {MaterialModule} from '../material.module';
import {RouterModule} from '@angular/router';



@NgModule({

  imports: [
    MaterialModule,
    CommonModule,
    RouterModule,
  ],
  declarations: [SideMenuComponent],
  exports: [
    SideMenuComponent
  ]
})
export class SideMenuModule { }
