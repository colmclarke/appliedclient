import { Component, OnInit } from '@angular/core';
import {MenuItem} from './menu-item';
import {Router} from '@angular/router';
@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss']
})
export class SideMenuComponent implements OnInit {
title = 'APPLIED';
menuItems: MenuItem[] = [
  {
    Name: 'Create New Policy',
    Url: '/drivers',
    Icon: 'control_point',
    Description: 'Create a new insurance policy'
  },
];
  constructor(public router: Router) { }
  ngOnInit() {
  }
}
