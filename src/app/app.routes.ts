import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {PolicyComponent} from './policy/policy.component';

const routes: Routes = [
  {path: '', component: PolicyComponent},
  {path: 'test', component: PolicyComponent},
  {path: 'drivers', component: PolicyComponent},
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      routes,
      {enableTracing: false}
    )
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
