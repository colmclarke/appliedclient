import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MaterialModule} from './material.module';
import {PolicyModule} from './policy/policy.module';
import {SideMenuModule} from './side-menu/side-menu.module';
import {PremuimCalculatorService} from './policy/premuim-calculator.service';
import {HttpClientModule} from '@angular/common/http';
import {AppRoutingModule} from './app.routes';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    HttpClientModule,
    MaterialModule,
    BrowserModule,
    AppRoutingModule,
    PolicyModule,
    BrowserAnimationsModule,
    SideMenuModule
  ],
  entryComponents: [],
  providers: [PremuimCalculatorService],
  bootstrap: [AppComponent]
})
export class AppModule { }
