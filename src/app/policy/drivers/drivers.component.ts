import {Component} from '@angular/core';
import {PremuimCalculatorService} from '../premuim-calculator.service';
import {PolicyService} from '../policy.service';

import {takeUntil} from 'rxjs/operators';
import {DriverModel} from '../driver.model';
import {FormControl, FormGroup} from '@angular/forms';
import * as moment from 'moment';
import {Subject} from 'rxjs';
import {HttpErrorResponse} from '@angular/common/http';

export interface Occupation {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-drivers',
  templateUrl: './drivers.component.html'
})
export class DriversComponent {
  private unsubscribe: Subject<void> = new Subject<void>();
  driver = new DriverModel();
  private drivers: DriverModel[] = [];
  private startDate;
  private premiumAmount: number;
  selectedOccupation: string;
  occupations: Occupation[] = [
    {value: 'accountant', viewValue: 'Accountant'},
    {value: 'chauffeur', viewValue: 'Chauffeur'},
  ];
  policyForm: FormGroup = new FormGroup({
    startDate: new FormControl(),
    name: new FormControl(),
    occupation: new FormControl(),
    birthDate: new FormControl(),
  });

  constructor(private calculatePremService: PremuimCalculatorService, private policyService: PolicyService) {
  }

  getQueryString(): string {
    const queryParams: string[] = [];

    this.processDateQuery(queryParams, 'startDate');
    this.processStringQuery(queryParams, 'name');
    this.processStringQuery(queryParams, 'occupation');
    this.processDateQuery(queryParams, 'birthDate');
    return queryParams.join('&');
  }

  private processStringQuery(queryParams: string[], fieldName: string): void {
    const value: string = this.getFormValue(fieldName);

    if (value !== null) {
      const queryParam = this.formatQueryParam(fieldName, value);
      queryParams.push(queryParam);
    }
  }

  private processDateQuery(queryParams: string[], fieldName: string): void {
    const value: moment.Moment = this.getFormValue(fieldName);

    if (value !== null) {
      const date = this.formatDate(value);
      const queryParam = this.formatQueryParam(fieldName, date);
      queryParams.push(queryParam);
    }
  }

  private formatDate(date: moment.Moment): string {
    return date.format('DD/MM/YYYY');
  }

  private formatQueryParam(fieldName: string, value: string): string {
    return fieldName + '=' + value;
  }

  private getFormValue(fieldName: string): any {
    return this.policyForm.get(fieldName).value;
  }

  private setFormValue(fieldName: string, value: any): void {
    this.policyForm.get(fieldName).setValue(value);
  }

  bindFormToService() {
    this.policyService.setPolicyStart(this.getFormValue('startDate'));
    this.policyService.setDriverName(this.getFormValue('name'));
    this.policyService.setDriverOccupation(this.getFormValue('occupation'));
    this.policyService.setDriverBirthDate(this.getFormValue('birthDate'));
  }

  selectChangeHandler(event: any) {
    this.selectedOccupation = event.target.value;
  }

  onSubmit() {
    this.bindFormToService();
    const queryString = this.getQueryString();
    console.log(this.getQueryString());
    this.calculatePremService.fetchQuote(queryString)
      .pipe(takeUntil(this.unsubscribe))
      .subscribe((response: number) => {
        this.policyService.setPremiumAmount(this.premiumAmount);
        this.premiumAmount = response;
        this.policyService.setPremiumAmount(this.premiumAmount);
      }, (error: HttpErrorResponse) => {
        alert(error.error);
        console.log(error);
      });
    this.policyService.setPremiumAmount(this.premiumAmount);
  }

}

