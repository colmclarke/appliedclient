import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {catchError, retry} from 'rxjs/operators';
import {error} from 'util';


@Injectable()
export class PremuimCalculatorService {
  api = 'https://localhost:5001/quote';
  constructor(private http: HttpClient) {
  }
  fetchQuote(query: string): Observable<number> {
    return this.http.get<number>(this.api + '?' + query);
  }

   openNewPolicy() {}
  amendPolisy() {}
  cancelPolicy() {}
}



