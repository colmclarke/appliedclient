import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PolicyComponent } from './policy.component';
import {MaterialModule} from '../material.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { QuoteComponent } from './quote/quote.component';
import { DriversComponent } from './drivers/drivers.component';
import {PolicyService} from './policy.service';

@NgModule({
  declarations: [PolicyComponent, QuoteComponent, DriversComponent],
  imports: [
    ReactiveFormsModule,
    MaterialModule,
    CommonModule,
    FormsModule
  ],
  providers: [PolicyService],
})
export class PolicyModule { }
