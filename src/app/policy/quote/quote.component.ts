import {Component, Input, OnInit} from '@angular/core';
import {PolicyService} from '../policy.service';
import DateTimeFormat = Intl.DateTimeFormat;

@Component({
  selector: 'app-quote',
  templateUrl: './quote.component.html',
  styleUrls: ['./quote.component.scss']
})
export class QuoteComponent implements OnInit {
policyType = 'value';
driverName: string;
driverOccupation: string;
driverBirthDate: string;
premuimAmount: number;

constructor(public policyService: PolicyService) {
  this.driverName = policyService.getDriverName();
  this.driverOccupation = policyService.getDriverOccupation();
  this.driverBirthDate = '';
  this.premuimAmount = policyService.getPremiumAmount();

}

  ngOnInit() {
    this.driverName = this.policyService.getDriverName();
  }
  update() {
    this.driverName = this.policyService.getDriverName();
    this.driverOccupation = this.policyService.getDriverOccupation();
    this.driverBirthDate = '';
    this.premuimAmount = this.policyService.getPremiumAmount();
  }

}
