import { NgModule } from '@angular/core';
import {MatMomentDateModule} from '@angular/material-moment-adapter';
import {
  MatNativeDateModule,
  MatGridListModule,
  MatTableModule,
  MatToolbarModule,
  MatDatepickerModule,
  MatInputModule,
  MatChipsModule,
  MatButtonModule,
  MatFormFieldModule,
  MatSelectModule,
  MatMenuModule,
  MatIconModule,
  MatCardModule,
  MatDividerModule,
  MatRippleModule,
  MatSidenavModule,
  MatExpansionModule,
  MatButtonToggleModule,
  MatCheckboxModule, MatTabsModule, MatDialogModule, MAT_DATE_LOCALE
} from '@angular/material';
import {FlexLayoutModule} from '@angular/flex-layout';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';



@NgModule({
  imports: [
    MatChipsModule,
    MatDialogModule,
    MatNativeDateModule,
    FlexLayoutModule,
    MatTabsModule,
    MatTableModule,
    MatCheckboxModule,
    MatButtonToggleModule,
    MatGridListModule,
    MatExpansionModule,
    MatSidenavModule,
    MatRippleModule,
    MatDividerModule,
    MatTableModule,
    MatToolbarModule,
    MatDatepickerModule,
    MatInputModule,
    MatButtonModule,
    MatMenuModule,
    MatFormFieldModule,
    MatSelectModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatMomentDateModule,
    BrowserAnimationsModule
  ],
  exports: [
    MatChipsModule,
    MatDialogModule,
    MatNativeDateModule,
    FlexLayoutModule,
    MatTabsModule,
    MatTableModule,
    MatCheckboxModule,
    MatButtonToggleModule,
    MatGridListModule,
    MatExpansionModule,
    MatSidenavModule,
    MatRippleModule,
    MatDividerModule,
    MatTableModule,
    MatToolbarModule,
    MatDatepickerModule,
    MatInputModule,
    MatButtonModule,
    MatMenuModule,
    MatFormFieldModule,
    MatSelectModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatMomentDateModule,
    BrowserAnimationsModule
  ],
  providers: [
    {provide: MAT_DATE_LOCALE, useValue: 'en-GB'}
  ]
})
export class MaterialModule {}
