import { Component } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'applied-client';
  currentRoute = 'New Policy';

  constructor(private router: Router) {
    // this.currentRoute = this.router.url.substr(1);
  }
}
