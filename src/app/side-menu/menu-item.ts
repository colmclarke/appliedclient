export class MenuItem {
  Name: string;
  Url: string;
  Icon: string;
  Description: string;
}
