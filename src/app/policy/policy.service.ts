import {DriverModel} from './driver.model';
import DateTimeFormat = Intl.DateTimeFormat;

export class PolicyService {
  private driver: DriverModel = new DriverModel();
  private policyStartDate: DateTimeFormat;
  private premiumAmount: number;

  getDriverName(): string {
    if (this.driver.name) {
      return this.driver.name;
    }
  }
  setDriverName(value: string) {
    console.log(value);
    if (value !== null || value !== undefined) {
      this.driver.name = value;
    }
  }

  getDriverOccupation(): string {
    if (this.driver.occupation) {
      return this.driver.occupation;
    }
  }
  setDriverOccupation(value: string) {
    console.log(value);
    return this.driver.occupation = value;
  }

  getDriverBirthDate(): DateTimeFormat {
    if (this.driver.birthDate) {
      return this.driver.birthDate;
    }
  }
  setDriverBirthDate(value: DateTimeFormat) {
    console.log(value);
    return this.driver.birthDate = value;
  }

  getPolicyStart(): DateTimeFormat {
    if (this.policyStartDate) {
      return this.policyStartDate;
    }
  }
  setPolicyStart(value: DateTimeFormat) {
    console.log(value);
    return this.policyStartDate = value;
  }

  getPremiumAmount(): number {
    if (this.driver.name) {
      return this.premiumAmount;
    }
  }
  setPremiumAmount(value: number) {
    console.log('Premium amount set at: ' + value);
    return this.premiumAmount = value;
  }
}
